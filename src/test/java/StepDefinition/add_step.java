package StepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by rk on 3/14/2017.
 */
public class add_step {
    int a;
    int b;
    int c;

    @Given("^two numbers$")
    public void two_number()
    {
        a = 1;
        b = 2;
    }

    @When("^number one added to number two$")
    public void adding_twonumbers()
    {
        c = a+b;
    }

    @Then("^result should be three$")
    public void result()
    {
        System.out.print(c);
    }
}
