package StepDefinition;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by rk on 3/21/2017.
 */
public class google_steps {

    WebDriver driver;

    @Given("^a browser")
    public void aBrowser()  {
        // Write code here that turns the phrase above into concrete actions

        System.setProperty("webdriver.gecko.driver", "C:\\Selenium\\geckodriver-v0.15.0-win64\\geckodriver.exe");

        driver = new FirefoxDriver();

//        throw new PendingException();
    }

    @When("^google url is provided$")
    public void googleUrlIsProvided() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        driver.navigate().to("https://www.google.com/?gws_rd=ssl");

//        throw new PendingException();
    }

    @Then("^the text should be searched$")
    public void theTextShouldBeSearched() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        driver.findElement(By.id("lst-ib")).sendKeys("hi");

        //throw new PendingException();
    }
}
